'use babel';

import { CompositeDisposable } from 'atom';
import { Point } from 'atom';
import { Range } from 'atom';


export default {
    subscriptions: null,

    activate(state) {
        // Events subscribed to in atom's system can be easily cleaned up with a
        // CompositeDisposable
        this.subscriptions = new CompositeDisposable();

        // Register command that toggles this view
        this.subscriptions.add(atom.commands.add('atom-workspace', {
            'comment-formatting:divider_title': () => this.divider_title(),
            'comment-formatting:seperator_title': () => this.seperator_title(),
            'comment-formatting:add_seperator': () => this.add_seperator(),
            'comment-formatting:space': () => this.space()
        }));
    },

    deactivate() {
        this.subscriptions.dispose();
    },

    divider_title() {
        let editor;
        if (editor = atom.workspace.getActiveTextEditor()) {
            let cursor_pos = editor.getCursorBufferPosition();
            let comment =  this.get_comment();
            let text = this.get_text();
            let line_length = this.get_line_length();
            let indentation = editor.indentationForBufferRow(
                editor.getCursorBufferPosition().row
            );
            let indentation_length = indentation * editor.getTabLength();
            let max_length = atom.config.get('editor.preferredLineLength');
            // TODO: how to handle selection longer than line length
            let offset =
                ((max_length - comment.length - indentation_length) / 2) -
                (text.length / 2);
            let formatted_comment = this.get_seperator(indentation) + '\n';
            formatted_comment += this.build_indentation(indentation);
            formatted_comment += comment;
            for(var i = 0; i < offset; i++) {
                formatted_comment += ' ';
            }
            formatted_comment += text + '\n';
            formatted_comment += this.get_seperator(indentation);
            let checkpoint = editor.createCheckpoint();
            editor.setSelectedBufferRange(new Range(
                new Point(cursor_pos.row, 0),
                new Point(cursor_pos.row, line_length)
            ));
            editor.insertText(formatted_comment);
            editor.groupChangesSinceCheckpoint(checkpoint);
        }
    },

    seperator_title() {
        let editor;
        if (editor = atom.workspace.getActiveTextEditor()) {
            let cursor_pos = editor.getCursorBufferPosition();
            let comment =  this.get_comment();
            let text = this.get_text();
            let line_length = this.get_line_length();
            let indentation = editor.indentationForBufferRow(
                editor.getCursorBufferPosition().row
            );
            let indentation_length = indentation * editor.getTabLength();
            let max_length = atom.config.get('editor.preferredLineLength');
            // TODO: how to handle selection longer than line length
            let offset =
                ((max_length - comment.length - indentation_length) / 2) -
                (text.length / 2);
            let indentation_str = this.build_indentation(indentation);
            let formatted_comment = '';
            formatted_comment += comment;
            for(var i = 0; i < offset; i++) {
                formatted_comment += '-';
            }
            formatted_comment += text;
            let char_length = max_length - indentation_length;
            while(formatted_comment.length < char_length) {
                formatted_comment += '-';
            }
            let checkpoint = editor.createCheckpoint();
            editor.setSelectedBufferRange(new Range(
                new Point(cursor_pos.row, 0),
                new Point(cursor_pos.row, line_length)
            ));
            editor.insertText(indentation_str + formatted_comment);
            editor.groupChangesSinceCheckpoint(checkpoint);
        }
    },

    add_seperator() {
        let editor;
        if (editor = atom.workspace.getActiveTextEditor()) {
            let cursor_pos = editor.getCursorBufferPosition();
            let text = this.get_text();
            let line_length = this.get_line_length();
            let indentation = editor.indentationForBufferRow(
                editor.getCursorBufferPosition().row
            );
            let checkpoint = editor.createCheckpoint();
            if(text.length > 0) {
                editor.insertNewlineAbove();
                editor.setSelectedBufferRange(new Range(
                    new Point(cursor_pos.row, 0),
                    new Point(cursor_pos.row, 0)
                ));
            }
            else {
                editor.setSelectedBufferRange(new Range(
                    new Point(cursor_pos.row, 0),
                    new Point(cursor_pos.row, line_length)
                ));
            }
            editor.insertText(this.get_seperator(indentation));
            editor.groupChangesSinceCheckpoint(checkpoint);
        }
    },

    space() {
        let editor;
        if (editor = atom.workspace.getActiveTextEditor()) {
            let cursor_pos = editor.getCursorBufferPosition();
            let text = this.get_text();
            let line_length = this.get_line_length();
            let indentation = editor.indentationForBufferRow(
                editor.getCursorBufferPosition().row
            );
            let spaced = this.build_indentation(indentation);
            for(var i = 0; i < text.length - 1; i++) {
                spaced += text[i] + ' ';
            }
            spaced += text[i];
            let checkpoint = editor.createCheckpoint();
            editor.setSelectedBufferRange(new Range(
                new Point(cursor_pos.row, 0),
                new Point(cursor_pos.row, line_length)
            ));
            editor.insertText(spaced);
            editor.groupChangesSinceCheckpoint(checkpoint);
        }
    },

    // TODO: support cursors - iterator over cursor positions and pass them
    //       to this function
    get_text() {
        let editor;
        if (editor = atom.workspace.getActiveTextEditor()) {
            return editor.lineTextForBufferRow(
                editor.getCursorBufferPosition().row
            ).trim();
        }
        return '';
    },

    get_line_length() {
        let editor;
        if (editor = atom.workspace.getActiveTextEditor()) {
            return editor.lineTextForBufferRow(
                editor.getCursorBufferPosition().row
            ).length;
        }
        return 0;
    },

    build_indentation(indentation) {
        let editor;
        if (editor = atom.workspace.getActiveTextEditor()) {
            // construct the tab string
            tab_str = '';
            if(editor.getSoftTabs()) {
                for(var i = 0; i < editor.getTabLength(); ++i) {
                    tab_str += ' ';
                }
            }
            else {
                tab_str = '\t';
            }
            // add indentation
            let str = '';
            for(var i = 0; i < indentation; ++i) {
                str += tab_str;
            }
            return str;
        }
        return '';
    },

    get_comment() {
        let editor;
        if (editor = atom.workspace.getActiveTextEditor()) {
            let grammar = editor.getGrammar();
            if (typeof(grammar.commentStrings) !== 'undefined') {
                if (typeof(grammar.commentStrings.commentStartString) !==
                    'undefined') {
                    return grammar.commentStrings.commentStartString;
                }
            }
        }
        return '// ';
    },

    get_seperator(indentation) {
        let editor;
        if (editor = atom.workspace.getActiveTextEditor()) {
            let max_length = atom.config.get('editor.preferredLineLength');
            let sep = this.get_comment();
            let char_length =
                max_length - (indentation * editor.getTabLength());
            while(sep.length < char_length) {
                sep += '-';
            }
            sep = this.build_indentation(indentation) + sep;
            return sep;
        }
        return '';
    }
};
